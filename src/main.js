import Vue from 'vue';
import App from './App.vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue);
Vue.use(VueRouter);

import Institutions from './pages/Institutions';
import About from './pages/About';
const routes = [
  { path: '/', redirect: { name:"Institutions"}},
  { 
    path: '/institutions',
    name: "Institutions",
    component: Institutions,
  },
  { path: '/about', component: About}
];

const router = new VueRouter({
  routes: routes, 
  mode: 'history'
})

Vue.config.productionTip = false

new Vue({
  el: "#app",
  template: '<App/>',
  components: {App},
  render: h => h(App),
  router
}).$mount('#app')

