

var SimpleApi = {
    API: {
        url: '/institutions',
        get: (url) => {
            return fetch("/institutions/" + url)
                   .then(response => response.json())
        },
        Institutions: {
            listAll: () => {
                return SimpleApi.API.get('')
            }
        }
    }
};

export default SimpleApi