# app

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```


### Api Endpoints

```
https://min-api.cryptocompare.com/data/pricemulti?fsyms=BTC,ETH,BCH,LTC&tsyms=USD,EUR
```